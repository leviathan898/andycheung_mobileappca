Hi John,

I have published the Android version of my CA (signed) as the apk included in this file. I am only submitting the www folder as you said, and I'm sure you'll see from the app.js what plugins you'll need. The only one that I used that isn't one we've used before is the ngRoute plugin. I've left comments throughtout the code for you to see as well. I've also shared the Ionic View with your email, and shared it with you on Bitbucket. I've also installed the Android build to the project. Hopefully I haven't left anything out.

Cheers for everything, I've really enjoyed the self-learning part of this module, though it's a shame we didn't have more time for Java and exploring what else we could do with mobile apps! Let me know if there's anything else.

Andy

https://bitbucket.org/leviathan898/andycheung_mobileappca
https://drive.google.com/open?id=0B6sGvcqyS764b0l3b1g4UnRQZUU

App email : andyCheung@email.com
App password : Softshellcrab3