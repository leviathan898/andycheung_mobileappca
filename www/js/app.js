
angular.module('starter', ['ionic', 
                           'starter.controllers.login',
                           'starter.services.auth',
                           'tracker.controllers',
                           'tracker.services',
                           'fb.tracker.controllers',
                           'fb.tracker.services',
                           'ngMessages',
                           'ngCordova',
                           'ngMap',
                           'firebase',
                           //injected ngRoute plugin to use refresh tools
                           'ngRoute'])

.run(function($ionicPlatform, $rootScope, $state) {
	
	$rootScope.$on("$stateChangeError", function(event, toState, toParams, fromState, fromParams, error) {

	    if (error === "AUTH_REQUIRED") {
	    	console.log("login required")
	      $state.go("login");
	    }
	  });
	
  $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider


    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

.state('login', {
              url: '/login',
              templateUrl: 'module/user/login.html',
              controller: 'LoginCtrl',
})

.state('tab.signout', {
              url: '/signout',
              views: {
                  'tab-signout': {
                    controller: 'SignoutCtrl',
                   
                  }
                }
})

  .state('tab.tracker', {
      url: '/tracker',
      views: {
        'tab-tracker': {
          templateUrl: 'module/tracker/tab-tracker.html',
          controller: 'TrackerCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        	  } // currentAuth
          } // resolve
        }
      }
    })
    
    
    
  .state('tab.fbtracker', {
      url: '/fbtracker',
      views: {
        'tab-fbtracker': {
          templateUrl: 'module/fbtracker/tab-tracker.html',
          controller: 'FBTrackerCtrl',
          
          //https://github.com/firebase/angularfire/blob/master/docs/guide/user-auth.md#ui-router-example
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        	  } // currentAuth
          } // resolve
        }
      }
    })
    
      .state('tab.addTrackedItemToFB', {
      url: '/addTrackedItemToFB',
      views: {
        'tab-fbtracker': {
          templateUrl: 'module/fbtracker/add-trackedItem.html',
          controller: 'AddTrackedItemToFBCtrl',
          
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        	  } // currentAuth
          } // resolve
        }
      }
    })
    
       .state('tab.fbItemDetail', {
      url: '/fbItemDetail/:fbItemId',
      views: {
        'tab-fbtracker': {
          templateUrl: 'module/fbtracker/item-detail.html',
          controller: 'fbItemDetailCtrl',
          
          resolve : { 
        	  currentAuth :  function (Auth) {
        		  
        		   return Auth.$requireSignIn();
        	  } // currentAuth
          } // resolve
        }
      }
    })
    
  // deleted dash and used tracker tab as default
  $urlRouterProvider.otherwise('/tab/tracker');
});
