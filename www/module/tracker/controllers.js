angular.module('tracker.controllers', [])
.controller('TrackerCtrl', function($scope, TrackerService, $cordovaGeolocation,  $ionicPlatform) {
	
	  $scope.title ="Tracker List"
	  $scope.trackedItems = TrackerService.load();
	  $scope.trackerItem = {latitude : "", longitude : ""}
	  
$ionicPlatform.ready(function() {
		  var posOptions = {timeout: 10000, enableHighAccuracy: true};
		  $cordovaGeolocation.getCurrentPosition(posOptions)
		    .then(function(position) {
		    	$scope.coords = position.coords;
		    	
		    	$scope.trackerItem.latitude = $scope.coords.latitude;
		    	
		    	$scope.trackerItem.longitude = $scope.coords.longitude;
			})
})
	  
	  $scope.$on('mapInitialized', function (event,map) {

        $scope.map = map;

    });
	  
})
