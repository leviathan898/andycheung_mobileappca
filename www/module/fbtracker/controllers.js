angular.module('fb.tracker.controllers', [])

.controller('FBTrackerCtrl', function($scope, $firebaseArray ) {
	
	  $scope.title ="FB Tracker List"
		  
		  
	  var ref = firebase.database().ref().child("trackedItems");
	  $scope.trackedItems = $firebaseArray(ref);  
	  
	  console.log($scope.trackedItems);
	  
})

.controller('AddTrackedItemToFBCtrl', function($scope, $state, FBTrackerService, $cordovaGeolocation,  $ionicPlatform, $firebaseArray, $ionicPopup,$cordovaCamera) {

	$scope.bikeStations = FBTrackerService.load();
	
	//to get date and time of journey
	var currentdate = new Date(); 
var datetime = currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();

	//created start and finish properties to hold coords for start point and end point of journey, datetime for moment of journey creation and imageData to hold image associated with this object
	$scope.trackerMasterItem = {title : "", description : "", latitude : "", longitude : "", start :"", finish : "", imageData : '', dateTime : datetime};
	
	$scope.trackerItem = {title : "", description : "", latitude : "", longitude : "", start :"", finish : "", imageData : '', dateTime : datetime};
	
	//gets current GPS position, object properties are populated with these coords
	$ionicPlatform.ready(function() {
		  var posOptions = {timeout: 10000, enableHighAccuracy: true};
		  $cordovaGeolocation.getCurrentPosition(posOptions)
		    .then(function(position) {
		    	$scope.coords = position.coords;
		    	
		    	$scope.trackerItem.latitude = $scope.coords.latitude;
		    	
		    	$scope.trackerItem.longitude = $scope.coords.longitude;
		    	
		    }, function(err) {
		    	console.log('getCurrentPosition error: ' + angular.toJson(err));
		    });
		});
		
	console.log("AddTrackedItemCtrl::addTrackerItem");
	
	
	var ref = firebase.database().ref().child("trackedItems");
	  // create a synchronized array
	 $scope.trackedItems = $firebaseArray(ref);
	
	
	$scope.addTrackerItem = function (form) {
		
		if (form.$valid) {

			$scope.trackedItems.$add(angular.copy($scope.trackerItem));
		
			//Reset trackerItem
			$scope.trackerItem = angular.copy($scope.trackerMasterItem);
			
			
			$state.go("tab.fbtracker")
			
		} else {
			
			console.log("TrackerCtrl::invalidForm");
			console.log(form.$error);
		}
	}
	
	//put method to initialise camera and return image data here in order to submit with other form fields
$scope.addPicture = function () {
		
		 
		 $scope.message = "Taking picture " + Date().toString();
		 
		 $scope.imageData = "Waiting for image data";

		 var options = {
		                   quality: 50,
		                   destinationType: Camera.DestinationType.DATA_URL,
		                   sourceType: Camera.PictureSourceType.CAMERA, // CAMERA
		                   allowEdit: true,
		                   encodingType: Camera.EncodingType.JPEG,
		                   targetWidth: 480,
		                   popoverOptions: CameraPopoverOptions,
		                   saveToPhotoAlbum: false
		             };



		     $cordovaCamera.getPicture(options).then(
		       function(imageData){
		    	  
		           $scope.trackerItem.imageData = imageData;

		       });
		 }

})

//passed in window to use refresh tools
.controller('fbItemDetailCtrl',function ($scope, $stateParams, $firebaseObject,$cordovaGeolocation, $ionicPlatform, $window) {
	
	//method to reload current page
	$scope.reloadRoute = function() {
		$window.location.reload();
	}


	$scope.$on('mapInitialized', function (event,map) {

        $scope.map = map;

    });
	
	$scope.item = $firebaseObject(firebase.database().ref().child("trackedItems").child($stateParams.fbItemId));

});