angular.module('fb.tracker.services', [])
.factory('FBTrackerService', function($q, $http) {
	
	  var trackedItems = [];
		var bikeStations = [];

	  return {
	    all: function() {
	      return trackedItems;
	    },
	    
	    add: function(trackedItem) {
	    	//trackedItems.push(trackedItem)
	    	
	    	var ref = firebase.database().ref();
	    	  // download the data into a local object
		},
	    
	    remove: function(trackedItem) {
	    	trackedItems.splice(trackedItems.indexOf(trackedItem), 1);
	    },
	    get: function(trackedItemId) {
	      for (var i = 0; i < trackedItems.length; i++) {
	        if (trackedItems[i].id === parseInt(trackedItemId)) {
	          return trackedItems[i];
	        }
	      }
	      return null;
	    },

			load : function () {
                var deferred = $q.defer();

                  var bikeServiceUrl = "https://api.jcdecaux.com/vls/v1/stations?contract=dublin&apiKey=4e15d64cf75cd1c723a9e4f9e2665ff5ddbf2bd7";
                  
                  $http.get(bikeServiceUrl, {}).success( function (data) {


          console.log("loading", bikeServiceUrl, data);

          angular.forEach(data, function(bikeStation) {
            
            bikeStations.push(bikeStation);
                           
          });
          deferred.resolve();

        }).error( function () {

              alert('there was an error');
              });


              return bikeStations;

            }
	  };
});