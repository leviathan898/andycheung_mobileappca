var app = angular.module("starter.services.auth", ["firebase"]);

app.factory("Auth", 
  function($firebaseAuth) {
    return $firebaseAuth();
  }
);